@0x8b246d21c250872b;

interface HelloWorld  {
	struct HelloRequest {
		name @0 :Text;
	}

	struct HelloReply {
		message @0 :Text;
	}

	sayHello @0 (request: HelloRequest) -> (reply: HelloReply);
}
