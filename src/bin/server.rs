use async_std::{net::TcpListener, stream::StreamExt, task::Builder};
use capnp::capability::Promise;
use capnp_rpc::{pry, rpc_twoparty_capnp, twoparty, RpcSystem};
use capnproto_example::hello_world_capnp::hello_world;

struct HelloWorldImpl;

impl hello_world::Server for HelloWorldImpl {
    fn say_hello(
        &mut self,
        params: hello_world::SayHelloParams,
        mut results: hello_world::SayHelloResults,
    ) -> Promise<(), ::capnp::Error> {
        let request = pry!(pry!(params.get()).get_request());
        let name = pry!(request.get_name());
        let message = format!("hello, {name}");

        results.get().init_reply().set_message(&message);

        Promise::ok(())
    }
}

#[async_std::main]
async fn main() -> async_std::io::Result<()> {
    let hello_world_client: hello_world::Client = capnp_rpc::new_client(HelloWorldImpl);

    let listener = TcpListener::bind("127.0.0.1:50070")
        .await
        .expect("failed to open listener");

    let mut incoming = listener.incoming();

    while let Some(stream) = incoming.next().await {
        let stream = stream.expect("failed to unwrap stream");

        let reader = stream.clone();
        let writer = stream;

        let network = twoparty::VatNetwork::new(
            reader,
            writer,
            rpc_twoparty_capnp::Side::Server,
            Default::default(),
        );

        let rpc_system = RpcSystem::new(Box::new(network), Some(hello_world_client.clone().client));

        Builder::new().local(rpc_system).expect("");
    }

    Ok(())
}
