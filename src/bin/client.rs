use async_std::{net::TcpStream, task::Builder};
use capnp_rpc::{rpc_twoparty_capnp, twoparty, RpcSystem};
use capnproto_example::hello_world_capnp::hello_world;

#[async_std::main]
async fn main() -> async_std::io::Result<()> {
    let stream = TcpStream::connect("127.0.0.1:50070").await?;
    stream.set_nodelay(true)?;

    let reader = stream.clone();
    let writer = stream;

    let rpc_network = Box::new(twoparty::VatNetwork::new(
        reader,
        writer,
        rpc_twoparty_capnp::Side::Client,
        Default::default(),
    ));

    let mut rpc_system = RpcSystem::new(rpc_network, None);

    let hello_world: hello_world::Client = rpc_system.bootstrap(rpc_twoparty_capnp::Side::Server);

    Builder::new().local(rpc_system).expect("");

    let mut request = hello_world.say_hello_request();
    request.get().init_request().set_name("hola rpc");

    let reply = request.send().promise.await.unwrap();

    println!("{:#?}", reply.get().unwrap().get_reply().unwrap());

    Ok(())
}
